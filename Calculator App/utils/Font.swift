//
//  Font.swift
//  Calculator App
//
//  Created by shahnazmul47@mgil.com on 7/6/21.
//

import Foundation


var fontPoppinsBold = "Poppins-Bold"
var fontPoppinsRegular = "Poppins-Regular"
var fontPoppinsThin = "Poppins-Thin"
var fontBarlowSemiCondensedBold = "BarlowSemiCondensed-Bold"
var fontBarlowSemiCondensedRegular = "BarlowSemiCondensed-Regular"
var fontBarlowSemiCondensedThin = "BarlowSemiCondensed-Thin"



var fontSize17 = 17
var fontSize20 = 20
