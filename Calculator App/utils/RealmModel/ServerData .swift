
//
//  ServerData.swift
//  Calculator App
//
//  Created by shahnazmul47@mgil.com on 7/6/21.
//
import Foundation
import RealmSwift

final class ServerData: Object {

   
    @objc dynamic var id = Int()
    @objc dynamic var heart_rate = ""
    @objc dynamic var sleep_time = ""
    @objc dynamic var training_time = ""
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
}
