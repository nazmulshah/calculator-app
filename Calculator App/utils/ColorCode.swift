//
//  ColorCode.swift
//  Calculator App
//
//  Created by shahnazmul47@mgil.com on 7/6/21.
//

import Foundation

var whiteHexCode = "FFFFFF"
var blackHexCode = "000000"

var aliceBlueHexCode1 = "E6F7FF"
//var aliceBlueHexCode2 = "F1FAFF"
var aliceBlueHexCode2 = "F9FAFF"

var nenoBlueHexCode1 = "B9C5FE"
//var nenoBlueHexCode2 = "CDE4FF"
var nenoBlueHexCode2 = "F9FAFF"
var pinkHexCode1 = "FFC6C6"
//var pinkHexCode2 = "FEDFDF"
var pinkHexCode2 = "F9FAFF"
var purpleHexCode1 = "98FABA"
//var purpleHexCode2 = "CCFCDF"
var purpleHexCode2 = "F9FAFF"
var orangeHexColor = "FF8C00"
